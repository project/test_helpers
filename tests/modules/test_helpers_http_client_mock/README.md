# Test Helpers HTTP Client Mock

The module provides API for capturing outgoing HTTP requests and saving the
content to files in the `store` mode, and intercept outgoing requests and set
the saved content as the response in the `mock` mode.

See the test `tests/src/Nightwatch/Tests/httpClientMockTest.js` for a usage
example.
